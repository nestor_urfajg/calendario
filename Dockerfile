FROM ubuntu:latest

LABEL version="1"
LABEL description="Calendario"

ARG DEBIAN_FRONTEND=noninteractive

RUN apt update && apt install nginx -y

ADD https://gitlab.com/nestor_urfajg/calendario/-/raw/main/index.html /var/www/html
ADD https://gitlab.com/nestor_urfajg/calendario/-/raw/main/calendario.css /var/www/html
ADD https://gitlab.com/nestor_urfajg/calendario/-/raw/main/calendario.js /var/www/html

ADD https://gitlab.com/nestor_urfajg/calendario/-/raw/main/default /etc/nginx/sites-available
ADD https://gitlab.com/nestor_urfajg/calendario/-/raw/main/entrypoint.sh /

RUN chmod +x /entrypoint.sh 

ENV SITENAME Calendario

EXPOSE 80 443

ENTRYPOINT [/entrypoint.sh]

CMD ["nginx", "-g", "daemon off;"]

